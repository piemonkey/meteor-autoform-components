#AutoForm Components

This module contains a few reworked autoform components.

Installation
```
meteor install abate:autoform-components
```

---

# Available components

- `checkboxToggle`

- `checkboxGroupGrid`
  Display a checkbox group in a grid

- `inlineArray`
  Display an array of fields in a compact way (without panels)
  Depends of **fortawesome:fontawesome** .

- `inlineGroup`
  Inline all fields of a Group

- `inlineObject`
  Inline all fields of an Object

To change the default template for a give type use
`AutoForm.setDefaultTemplateForType('afArrayField', 'inlineArray')`

---

# Global Template helpers

- `getUserName(uid)` : given a user Id returns a string containing either
   the *user.profile.firstName* or *user.profile.lastName* or *user.emails[0].address*

- `getUserEmail(uid)` : given a user Id returns a string containing *user.emails[0].address*

- `$lt(a,b)` `$gt(a,b)` : comparison helpers

- `debug(value)`: print on the console the content of the value and the data context

## Weak Dependencies

- `formatDate(date)`: given a date return a formatted date as `moment(date).format("DD-MM-YY")`

- `fa(iconName,dataAction)`: return a html fragment for a fontawesome icon. Ex.
    <i data-action=dataAction class="fa fa-icon_name" aria-hidden="true"></i>


# Templates

- `insertUpdateTemplate`: AutoForm helper for insert/update form . Ex.
    {{> insertUpdateTemplate form=form data=data }}

    where form contains the following fields
    form =
      collection: CollectionObject
      update:
        id: autoformUpdateId (default : "update#{CollectionName}FormId")
        method: autoformUpdateMethod ( default : "#{CollectionName}.update")
        label: autoformButtonInsertLabel (default : "Update #{CollectionName}")
      insert:
        id: autoformInsertId (default : "insert#{CollectionName}FormId")
        method: autoformInsertMethod ( default : "#{CollectionName}.insert")
        label: autoformButtonInsertLabel (default : "New #{CollectionName}")

  Only the CollectionObject is manadatory.

the data attribute of the insertUpdateTemplate is not reactive. Therefore
when it is needed, we recommend the following use pattern

    {{#if Template.subscriptionsReady}}
      {{> insertUpdateTemplate form=form data=data }}
    {{/if}}

# Dependencies

- aldeed:autoform@6.0.0
- twbs:bootstrap
- fortawesome:fontawesome (weak)
- peppelg:bootstrap-3-modal (weak)
- momentjs:moment (weak)

---

Licence MIT . Copyright Pietro Abate
