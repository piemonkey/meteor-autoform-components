Template.registerHelper "debug", (optionalValue) ->
  console.log("Current Context")
  console.log("====================")
  console.log(this)

  if optionalValue
    console.log("Value")
    console.log("====================")
    console.log(optionalValue)

Template.registerHelper "getUserName", (Id) -> share.getUserName(Id)
Template.registerHelper "getRealName", (Id) -> share.getUserName(Id,true)
Template.registerHelper "getUserEmail", (Id) -> share.getUserEmail(Id)

Template.registerHelper "$lt", (a,b) -> a < b
Template.registerHelper "$lte", (a,b) -> a <= b
Template.registerHelper "$gt", (a,b) -> a > b
Template.registerHelper "$gte", (a,b) -> a >= b
Template.registerHelper "$eq", (a,b) -> a == b
Template.registerHelper "$neq", (a,b) -> a != b
Template.registerHelper "$and", (a,b) -> a && b
Template.registerHelper "$or", (a,b) -> a || b
Template.registerHelper "$len", (l) -> if l? then l.length else 0
Template.registerHelper "$incr", (i) -> i + 1
Template.registerHelper "$in", (i,l) -> if ( typeof l ==  'object') then (i in l) else (i == l)

if Package['momentjs:moment']
  Template.registerHelper "formatDate",(date) -> moment(date).format("DD-MM-YY")
  Template.registerHelper 'formatTime', (date) -> moment(date).format("H:mm")

if Package['fortawesome:fontawesome']
  Template.registerHelper "fa", (name,dataAction,dataId) ->
    action = if typeof dataAction == "string" then "data-action=\"#{dataAction}\"" else ""
    id = if typeof dataId == "string" then "data-id=\"#{dataId}\"" else ""
    Spacebars.SafeString (
      "<i class=\"fa fa-#{name}\" #{action} #{id} aria-hidden=\"true\"></i>"
    )
