Package.describe({
  name: 'abate:autoform-components',
  version: '0.0.1',
  summary: 'Aufoform components',
  git: '',
  documentation: 'README.md',
})

Npm.depends({
  jquery: '3.2.1',
  bootstrap: '4.0.0',
})


Package.onUse((api) => {
  api.versionsFrom('1.4')

  api.use([
    'coffeescript',
    'reactive-dict',
  ], ['client', 'server'])

  api.use([
    'templating',
    'aldeed:autoform@6.0.0',
    'aldeed:template-extension',
    'peppelg:bootstrap-3-modal', // this should be a weak dependency
    'fortawesome:fontawesome',
    'momentjs:moment',
    'universe:i18n',
    'universe:i18n-blaze',
    'underscore',
  ], ['client'])

  api.use([
    'fortawesome:fontawesome',
    'momentjs:moment',
  ], ['client'], { weak: true })

  api.addFiles([
    'helpers/global_helpers.coffee',
    // 'components/inlineArray.html',
    // 'components/inlineGroup.html',
    // 'components/inlineObject.html',
    // 'components/checkboxGroupGrid.html',
    // 'components/checkboxToggle.html',
    // 'components/components.coffee',
    'templates/insertUpdate.html',
    'templates/insertUpdate.coffee',
    'templates/multiCollection.html',
    'templates/multiCollection.coffee',
    'templates/modal.html',
    'templates/modal.coffee',
  ], 'client')

  api.addFiles([
    'helpers/global.coffee',
    'api.coffee',
  ], ['client', 'server'])

  api.addFiles(['i18n/en.i18n.json'], ['client'])

  api.export(['AutoFormComponents'])
})
