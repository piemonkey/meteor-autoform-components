
# if Package['peppelg:bootstrap-3-modal']
# TODO : Dump this and use rd010:bootstrap-master-modal instead
share.ModalShowWithTemplate = (template,data,title,size) ->
  Modal.show("ModalTemplate",{
    template: template,
    data: data,
    title: title,
    size: if size then size else 'lg'
    })
