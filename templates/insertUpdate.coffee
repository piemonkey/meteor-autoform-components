if !Array::last
  Array::last = ->
    @[@length - 1]

share.collectionName = (form) ->
  if form.collection
    name = form.collection._name.split(".").last()
    name = name.charAt(0).toUpperCase() + name.slice(1)

# the data of the form is updated once the form is submitted
Template.insertUpdateTemplate.onCreated () ->
  template = this
  template.reactiveData = new ReactiveVar({})
  form = template.data.form
  id =
    if form?.update?.id then form.update.id
    else
      name = share.collectionName(form)
      "Update#{name}FormId"
  template.autorun () ->
    doc = Template.currentData().data
    template.reactiveData.set(doc)
    AutoForm.addHooks([ id ],
      onSuccess: () ->
        if form.collection
          doc = form.collection.findOne(this.docId)
          template.reactiveData.set(doc)
    )

Template.insertUpdateTemplate.helpers
  'data': () -> Template.instance().reactiveData.get()
  'var': () -> Template.currentData().var
  'hiddenFields': () ->
    form = Template.currentData().form
    if form?.hiddenFields? then form.hiddenFields
  'fields': () ->
    form = Template.currentData().form
    if form?.fields? then form.fields
  'omitFields': () ->
    form = Template.currentData().form
    if form?.omitFields? then form.omitFields
  'insertLabel': () ->
    form = Template.currentData().form
    if form?.insert?.label then form.insert.label
    else
      name = share.collectionName(form)
      tr = i18n.__('abate:autoform-components','new')
      "#{tr} #{name}"
  'updateLabel': () ->
    form = Template.currentData().form
    if form?.update?.label then form.update.label
    else
      name = share.collectionName(form)
      tr = i18n.__('abate:autoform-components','update')
      "#{tr} #{name}"
  'formIdInsert': () ->
    form = Template.currentData().form
    if form?.insert?.id then form.insert.id
    else
      name = share.collectionName(form)
      "Insert#{name}FormId"
  'formIdUpdate': () ->
    form = Template.currentData().form
    if form?.update?.id then form.update.id
    else
      name = share.collectionName(form)
      "Update#{name}FormId"
  'methodNameInsert': () ->
    form = Template.currentData().form
    if form?.insert?.method then form.insert.method
    else
      if form.collection then "#{form.collection._name}.insert"
  'methodNameUpdate': () ->
    form = Template.currentData().form
    if form?.update?.method then form.update.method
    else
      if form.collection then "#{form.collection._name}.update"

Template.insertUpdateTemplate.events
  'click [data-action="remove"]': (event,template) ->
    Id = $(event.currentTarget).data('id')
    form = Template.currentData().form
    name = form.collection._name
    method = if form?.update?.method then form.update.method else name+".remove"
    Meteor.call method, Id
